package pkg.dev.potridersdriver.model;

import android.view.ViewGroup;

import pkg.dev.potridersdriver.activity.adapter.DrawerAdapter;

/**
 * Created by Seema on 27/11/18.
 */

public abstract class DrawerItem<T extends DrawerAdapter.ViewHolder> {

    protected boolean isChecked;

    public abstract T createViewHolder(ViewGroup parent);

    public abstract void bindViewHolder(T holder);

    public DrawerItem setChecked(boolean isChecked) {
        this.isChecked = isChecked;
        return this;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public boolean isSelectable() {
        return true;
    }

}
