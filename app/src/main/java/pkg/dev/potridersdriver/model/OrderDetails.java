package pkg.dev.potridersdriver.model;

/**
 * Created by Seema on 27/11/18.
 */

public class OrderDetails {

    private String id;
    private String title;
    private String name;
    private String date;
    private String status;

    public OrderDetails(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
