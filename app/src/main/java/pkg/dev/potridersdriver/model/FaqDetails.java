package pkg.dev.potridersdriver.model;

import java.io.Serializable;

/**
 * Created by Seema on 27/11/18.
 */

public class FaqDetails implements Serializable {

    private String id;
    private String title;
    private String desc;


    public FaqDetails(String id, String title, String desc) {
        this.id = id;
        this.title = title;
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
