package pkg.dev.potridersdriver.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Seema on 29/11/18.
 */

public class CurrentJourneyEvent {

    private String event = "BEGIN_JOURNEY";
    private LatLng currentLatLng;

    public LatLng getCurrentLatLng() {
        return currentLatLng;
    }

    public void setCurrentLatLng(LatLng currentLatLng) {
        this.currentLatLng = currentLatLng;
    }
}
