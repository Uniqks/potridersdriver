package pkg.dev.potridersdriver.activity.fragment;


import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.databinding.FragmentRequestDetailBinding;


public class RequestDetailFragment extends BaseFragment implements View.OnClickListener {


    FragmentRequestDetailBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_request_detail, container, false);







        initControls();
        return binding.getRoot();
    }


    /**
     *   Method is used to initialized all the controls..
     */
    private void initControls() {

        setToolbarIconTitle("New Request", false, true, false,
                false, false, false, false);

        binding.topLL.setVisibility(View.GONE);
        setAnimationView(binding.topLL);

    }

    public void setAnimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);

        binding.completeBTN.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.completeBTN:
                HomeFragment.isReq = true;
                mainActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setAnimationView(binding.topLL);
    }
}
