package pkg.dev.potridersdriver.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.viewActivity.Home;
import pkg.dev.potridersdriver.app.PotriderApp;
import pkg.dev.potridersdriver.custom.CustomProgressDialog;
import pkg.dev.potridersdriver.databinding.ActivityForgotPasswordBinding;
import spencerstudios.com.bungeelib.Bungee;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {


    ActivityForgotPasswordBinding binding;
    private PotriderApp mPotriderApp;
    private CustomProgressDialog mProgressDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);


        mPotriderApp = (PotriderApp) getApplication();
        mProgressDialog = new CustomProgressDialog().getInstance();





        initControls();
    }


    /**
     *   Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.topLL.setVisibility(View.GONE);
        setAnimationView(binding.topLL);

        binding.backIV.setOnClickListener(this);
        binding.submitBTN.setOnClickListener(this);
    }


    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mPotriderApp.isValidEmail(binding.emailET)) {
            return false;
        }

        if (mPotriderApp.isNetworkAvailable(ForgotPassword.this)) {
            //doLoginService();
        } else {
            mPotriderApp.makeToast(getString(R.string.no_internet_connection), ForgotPassword.this);
        }

        return true;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backIV:
                mPotriderApp.hideKeyboard(ForgotPassword.this);
                finish();
                Bungee.slideLeft(ForgotPassword.this);
                break;

            case R.id.submitBTN:
                mPotriderApp.hideKeyboard(ForgotPassword.this);
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(binding.submitBTN);

                if (checkValidation()) {

                    mProgressDialog.showProgress(ForgotPassword.this, "", true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            finish();
                            Bungee.slideLeft(ForgotPassword.this);
                        }
                    }, 3000);
                }
                mProgressDialog.dismiss();
                break;
        }
    }

    /**
     *  Method is used to initialized set animation...
     */
    public void setAnimationView(View mMainTopLl) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
        mMainTopLl.startAnimation(bottomUp);
        mMainTopLl.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setAnimationView(binding.topLL);
    }
}
