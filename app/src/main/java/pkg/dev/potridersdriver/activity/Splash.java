package pkg.dev.potridersdriver.activity;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.app.PotriderApp;
import pkg.dev.potridersdriver.databinding.ActivitySplashBinding;
import spencerstudios.com.bungeelib.Bungee;

public class Splash extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


    ActivitySplashBinding binding;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private PotriderApp mPotriderApp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);

        mPotriderApp = (PotriderApp) getApplication();
        mPotriderApp.checkLocationPermission(Splash.this);





        initControls();
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            setHandler();
        }else{
            EnableGPSAutoMatically();
        }
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        final ValueAnimator animator = ValueAnimator.ofFloat(0.0f, 1.0f);
        animator.setInterpolator(new LinearInterpolator());
        animator.setDuration(2500L);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final float progress = (float) animation.getAnimatedValue();
                final float width = binding.imageViewBg.getWidth();
                final float translationX = width * progress;
                binding.imageViewBg.setTranslationX(translationX);
                binding.imageViewBg2.setTranslationX(translationX - width);
            }
        });
        animator.start();
    }



    private void setHandler() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                /*if (!isReg) {
                    SplashAlertDialog dialog = new SplashAlertDialog();
                    dialog.show(getSupportFragmentManager(), "Alert Dialog");
                } else {

                     if (PreferenceUtil.getPref(Splash.this).getBoolean(PreferenceKeys.IS_STARTED, false)) {
                    if (PreferenceUtil.getPref(Splash.this).getBoolean(PreferenceKeys.IS_LOGIN, false)) {

                        startActivity(new Intent(Splash.this, MainActivity.class));
                        finish();
                        Bungee.slideLeft(Splash.this);

                    } else {
                        PreferenceUtil.getPref(Splash.this).edit().putBoolean(PreferenceKeys.IS_FROM_MAIN_ACTIVITY, false).apply();
                        startActivity(new Intent(Splash.this, Login.class));
                        finish();
                        Bungee.slideLeft(Splash.this);
                    }

                } else {
                    startActivity(new Intent(Splash.this, ChooseOptionActivity.class));
                    finish();
                    Bungee.slideLeft(Splash.this);
                }
                }*/

                startActivity(new Intent(Splash.this, SplaceAlert.class));
                finish();
                Bungee.slideLeft(Splash.this);


            }
        }, 3000);
    }




    public void EnableGPSAutoMatically() {
        GoogleApiClient googleApiClient = null;
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi
                    .checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result
                            .getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // toast("Success");
                            // All location settings are satisfied. The client can
                            // initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            //  toast("GPS is not on");
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(Splash.this, 1000);

                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            toast("Setting change not allowed");
                            // Location settings are not satisfied. However, we have
                            // no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1000) {
            if(resultCode == Activity.RESULT_OK){
                setHandler();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                finish();
            }
        }
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        toast("Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        toast("Failed");
    }
    private void toast(String message) {
        try {
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            Log.e("", "Window has been closed");
        }
    }
    

}
