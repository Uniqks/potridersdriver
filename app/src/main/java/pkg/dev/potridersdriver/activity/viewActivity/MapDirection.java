package pkg.dev.potridersdriver.activity.viewActivity;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.fragment.HomeFragment;
import pkg.dev.potridersdriver.custom.HttpConnection;
import pkg.dev.potridersdriver.custom.PathJSONParser;
import pkg.dev.potridersdriver.model.CurrentJourneyEvent;
import pkg.dev.potridersdriver.model.JourneyEventBus;

public class MapDirection extends AppCompatActivity implements OnMapReadyCallback,
        LocationListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {



    private static final LatLng RAJKOT = new LatLng(22.303894, 70.802162);
    private static final LatLng GANDHINAGAR = new LatLng(23.215635, 72.636940);
    private static final LatLng AHMEDABAD = new LatLng(27.733601, 81.158401);
    private static final LatLng BARODA = new LatLng(22.307159, 73.181221);
    private static final LatLng LOWER_MANHATTAN = new LatLng(40.722543, -73.998585);
    private static final LatLng BROOKLYN_BRIDGE = new LatLng(40.7057, -73.9964);
    private static final LatLng WALL_STREET = new LatLng(40.7064, -74.0094);

    GoogleMap mMap;
    final String TAG = "PathGoogleMapActivity";

    Location mLastLocation;
    Marker mCurrLocationMarker, mSelectedLocationMarker;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    protected LocationManager mLocationManager;
    boolean isGPSEnabled = false;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 100;
    private static final int PERMISSION_REQUEST_CODE = 1;

    int height = 140;
    int width = 90;

    private float v;
    private double lat, lng;
    private LatLng startPosition, endPosition;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_direction);






        initControls();
    }

    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(isGPSEnabled) {
            GetLatLan();
        }else {
            showSettingsAlert_gps();
        }

        if (ActivityCompat.checkSelfPermission(MapDirection.this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (MapDirection.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults)
            return;
        }

        if (ContextCompat.checkSelfPermission(MapDirection.this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MapDirection.this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(MapDirection.this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(MapDirection.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(MapDirection.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(final Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        BitmapDrawable bitmapdraw1 = (BitmapDrawable)getResources().getDrawable(R.drawable.driver_vehicle);
        Bitmap b1 = bitmapdraw1.getBitmap();
        Bitmap curMarker = Bitmap.createScaledBitmap(b1, width, height, false);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        // markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        //markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.trackmarker));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(curMarker));
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                return false;
            }
        });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                valueAnimator.setDuration(9000);
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        v = valueAnimator.getAnimatedFraction();
                        lng = v * GANDHINAGAR.longitude + (1 - v)
                                * BARODA.longitude;
                        lat = v * GANDHINAGAR.latitude + (1 - v)
                                * BARODA.latitude;
                        LatLng newPos = new LatLng(lat, lng);
                        CurrentJourneyEvent currentJourneyEvent = new CurrentJourneyEvent();
                        currentJourneyEvent.setCurrentLatLng(newPos);
                        JourneyEventBus.getInstance().setOnJourneyUpdate(currentJourneyEvent);
                        mCurrLocationMarker.setPosition(newPos);
                        mCurrLocationMarker.setAnchor(0.5f, 0.5f);
                        mCurrLocationMarker.setRotation(getBearing(GANDHINAGAR, newPos));
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition
                                (new CameraPosition.Builder().target(newPos)
                                        .zoom(6.5f).build()));
                    }
                });
                valueAnimator.start();
            }
        }, 5000);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(8));
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

            }
        });

        String url = getMapsApiDirectionsUrl();
        ReadTask downloadTask = new ReadTask();
        downloadTask.execute(url);

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }



    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    private void GetLatLan() {
        Log.e("GetLatLan","GetLatLan");
        checkPermission();
    }

    public boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(MapDirection.this, android.Manifest.permission.READ_PHONE_STATE);
        int result1 = ContextCompat.checkSelfPermission(MapDirection.this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int result2 = ContextCompat.checkSelfPermission(MapDirection.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED ){

            LocationManager locationManager=(LocationManager) MapDirection.this.getSystemService(LOCATION_SERVICE);
            Criteria criteria=new Criteria();
            String  bestProvider=locationManager.getBestProvider(criteria, true);
            Location location=locationManager.getLastKnownLocation(bestProvider);
            if(location!=null) {
                onLocationChanged(location);
            } else {
            }

            locationManager.requestLocationUpdates(bestProvider, 10, 0, (android.location.LocationListener) this);
            return true;

        } else {
            requestPermission();
            return false;
        }
    }
    private void requestPermission(){
        ActivityCompat.requestPermissions(MapDirection.this, new String[]{android.Manifest.permission.READ_PHONE_STATE,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_REQUEST_CODE);
    }


    protected void showSettingsAlert_gps() {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(MapDirection.this);
        alertDialog.setTitle("GPS is settings");
        alertDialog.setMessage("GPS has not been enabled. To locate better friends for the setup menu and activate the GPS.  ");
        alertDialog.setPositiveButton("settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }


    private String getMapsApiDirectionsUrl() {
        String origin = "origin="+ RAJKOT.latitude + "," + RAJKOT.latitude ;
        String waypoints = "waypoints=optimize:true|"
                + BARODA.latitude + "," + BARODA.longitude
                + "|" + "|" + AHMEDABAD.latitude + ","
                + AHMEDABAD.longitude;
        String destination = "destination=" + GANDHINAGAR.latitude + "," + GANDHINAGAR.longitude;
        String key = "key=" + getString(R.string.google_maps_key);

        String sensor = "sensor=false";
        String params = origin + "&" + destination + "&" + waypoints + "&" + key;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + params;
        String url1 = "https://maps.googleapis.com/maps/api/directions/json?origin=rajkot&destination=ahmedabad&" + key;


        //https://maps.googleapis.com/maps/api/directions/json?origin=sydney,au&destination=perth,au
        // &waypoints=via:-37.81223%2C144.96254%7Cvia:-34.92788%2C138.60008&key=YOUR_API_KEY


        System.out.println("get location url ---> " + url1 + "   ---    " + url);
        return url;
    }

    private void addMarkers() {
        if (mMap != null) {
            mMap.addMarker(new MarkerOptions().position(RAJKOT).title("Rajkot"));
            mMap.addMarker(new MarkerOptions().position(BARODA).title("Baroda"));
            mMap.addMarker(new MarkerOptions().position(AHMEDABAD).title("Ahemadabad"));
            mMap.addMarker(new MarkerOptions().position(GANDHINAGAR).title("Gandhinagar"));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        addMarkers();

        MarkerOptions options = new MarkerOptions();
        options.position(RAJKOT);
        options.position(BARODA);
        options.position(AHMEDABAD);
        options.position(GANDHINAGAR);
        googleMap.addMarker(options);
        String url = getMapsApiDirectionsUrl();
        ReadTask downloadTask = new ReadTask();
        downloadTask.execute(url);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(RAJKOT, 13));

        PolylineOptions rectOptions = new PolylineOptions();

        rectOptions =rectOptions.add(RAJKOT);
        rectOptions =rectOptions.add(BARODA);
        rectOptions =rectOptions.add(GANDHINAGAR);
        rectOptions =rectOptions.add(AHMEDABAD);
        rectOptions =rectOptions.width(10);
        rectOptions =rectOptions.color(getResources().getColor(R.color.colorMap));
        Polyline polyline = mMap.addPolyline(rectOptions);



        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(MapDirection.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }


    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);

            } catch (Exception e) {
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            for (int i = 0; i < routes.size(); i++) {
                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(10);
                polyLineOptions.color(Color.BLUE);
            }

            if (routes != null && routes.size() != 0) {
                mMap.addPolyline(polyLineOptions);
            } else {
                Toast.makeText(MapDirection.this, "no routes found", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
