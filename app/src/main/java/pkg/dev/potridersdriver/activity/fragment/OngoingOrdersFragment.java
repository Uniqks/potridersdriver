package pkg.dev.potridersdriver.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.databinding.FragmentOngoingOrdersBinding;
import pkg.dev.potridersdriver.model.OrderDetails;


public class OngoingOrdersFragment extends BaseFragment {


    FragmentOngoingOrdersBinding binding;
    private List<OrderDetails> mOrderDetails = new ArrayList<>();
    private OngoingListAdapter mAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ongoing_orders, container, false);





        initControls();
        setOrderListAdapter();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mOrderDetails.add(new OrderDetails("New"));
        mOrderDetails.add(new OrderDetails("Pending"));
        mOrderDetails.add(new OrderDetails("Processing"));

        binding.ordersRV.setHasFixedSize(true);
        binding.ordersRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
    }

    /**
     *  Method is used to initialized ongoing order list adapter...
     */
    private void setOrderListAdapter() {

        mAdapter = new OngoingListAdapter(mOrderDetails, OngoingOrdersFragment.this);
        binding.ordersRV.setAdapter(mAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setanimationView(binding.ordersRV);
            }
        }, 500);
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }



    /**
     *  Ongoing order list adapter....
     */
    public class OngoingListAdapter extends RecyclerView.Adapter<OngoingListAdapter.ViewHolder> {

        private List<OrderDetails> mData;
        private Fragment fragment;

        public OngoingListAdapter(List<OrderDetails> mData, Fragment fragment) {
            this.mData = mData;
            this.fragment = fragment;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_order_list, viewGroup, false);



            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

            if (mData.get(position).getStatus() != null && !mData.get(position).getStatus().equals("")) {
                holder.mStatusTxt.setText(mData.get(position).getStatus());
            } else {
            }

            holder.mMainLl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mainActivity.slidingRootNav.setMenuLocked(true);
                    mainActivity.pushFragment(new OrderDetailsFragment(), true, false, true,
                            OrderDetailsFragment.class.getSimpleName(), null, false);
                }
            });

            holder.trackDeliveryBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mainActivity.slidingRootNav.setMenuLocked(true);
                    mainActivity.pushFragment(new TrackDeliveryFragment(), true, false, true,
                            TrackDeliveryFragment.class.getSimpleName(), null, false);
                }
            });

            holder.addOnsBT.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.addonsLL.getVisibility() == View.GONE) {
                        setanimationView(holder.addonsLL);

                    } else if (holder.addonsLL.getVisibility() == View.VISIBLE) {
                        holder.addonsLL.setVisibility(View.GONE);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView mStatusTxt;
            LinearLayout mMainLl, addonsLL;
            Button trackDeliveryBT, addOnsBT;

            public ViewHolder(@NonNull View view) {
                super(view);

                mStatusTxt = view.findViewById(R.id.statusTV);
                mMainLl = view.findViewById(R.id.orderLL);
                trackDeliveryBT = view.findViewById(R.id.trackDeliveryBT);
                addOnsBT = view.findViewById(R.id.addOnsBT);
                addonsLL = view.findViewById(R.id.addonsLL);
            }
        }
    }

}
