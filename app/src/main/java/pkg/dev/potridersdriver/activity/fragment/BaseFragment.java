package pkg.dev.potridersdriver.activity.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.viewActivity.Home;

/**
 * Created by Seema on 27/11/18.
 */

public class BaseFragment extends Fragment {

    public Home mainActivity;
    public Dialog dialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mainActivity == null) {
            //noinspection UnnecessaryParentheses
            mainActivity = (Home) getActivity();
        }
        //initProgressDialog();

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                NewRequestDialog dialog = new NewRequestDialog();
                dialog.show(getFragmentManager(), "new request");
            }
        }, 2000);*/


    }

    public void setToolbarIconTitle(String title, boolean isDrawerIcon, boolean isBackIcon, boolean isFilterIcon,
                                    boolean isEditIcon, boolean isCartIcon, boolean isSaveIcon, boolean isLogo) {
        if (isDrawerIcon) {
            mainActivity.mMenuImg.setVisibility(View.VISIBLE);
        } else {
            mainActivity.mMenuImg.setVisibility(View.GONE);
        }
        if (isFilterIcon) {
            mainActivity.mFilterImg.setVisibility(View.VISIBLE);
        } else {
            mainActivity.mFilterImg.setVisibility(View.GONE);
        }
        if (isBackIcon) {
            mainActivity.mBackImg.setVisibility(View.VISIBLE);
        } else {
            mainActivity.mBackImg.setVisibility(View.GONE);
        }
        if (isCartIcon) {
            mainActivity.mCartImg.setVisibility(View.VISIBLE);
        } else {
            mainActivity.mCartImg.setVisibility(View.GONE);
        }
        if (isEditIcon) {
            mainActivity.mEditImg.setVisibility(View.VISIBLE);
        } else {
            mainActivity.mEditImg.setVisibility(View.GONE);
        }
        if (isSaveIcon) {
            mainActivity.mSaveImg.setVisibility(View.VISIBLE);
        } else {
            mainActivity.mSaveImg.setVisibility(View.GONE);
        }
        if (isLogo) {
            mainActivity.mLogoImg.setVisibility(View.VISIBLE);
        } else {
            mainActivity.mLogoImg.setVisibility(View.GONE);
        }
        if (title != null) {
            mainActivity.mTitleTxt.setVisibility(View.VISIBLE);
            mainActivity.mTitleTxt.setText(title);
        } else {
            mainActivity.mTitleTxt.setVisibility(View.GONE);
        }


    }

    public void displayToast(String message, int length) {
        Toast.makeText(mainActivity, message, length).show();
    }

    private void initProgressDialog() {
        dialog = new Dialog(mainActivity);
       // View view = LayoutInflater.from(mainActivity).inflate(R.layout.layout_progress_dialog, null);
        //dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //dialog.setMessage(mContext.getString(R.string.api_loader_msg));
        dialog.setCancelable(false);
    }

    public void showProgressDialog() {
        if (dialog != null && !dialog.isShowing())
            dialog.show();
    }

    public void hideProgressDialog() {
        if (dialog != null && dialog.isShowing()) dialog.dismiss();
    }


}

