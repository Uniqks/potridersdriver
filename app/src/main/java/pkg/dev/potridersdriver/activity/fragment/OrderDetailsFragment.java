package pkg.dev.potridersdriver.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.databinding.FragmentOrderDetailsBinding;


public class OrderDetailsFragment extends BaseFragment {


    FragmentOrderDetailsBinding binding;
    private String mGetOrderNo;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_details, container, false);





        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mGetOrderNo = "Order No. #9894";
        setToolbarIconTitle(mGetOrderNo, false, true, false,
                false, false, false, false);
    }



    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle(mGetOrderNo, false, true, false,
                    false, false, false, false);
        }
    }

}
