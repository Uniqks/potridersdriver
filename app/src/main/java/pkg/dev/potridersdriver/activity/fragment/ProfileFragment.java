package pkg.dev.potridersdriver.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.viewActivity.Home;
import pkg.dev.potridersdriver.app.PotriderApp;
import pkg.dev.potridersdriver.databinding.FragmentProfileBinding;
import spencerstudios.com.bungeelib.Bungee;


public class ProfileFragment extends BaseFragment implements View.OnClickListener {


    FragmentProfileBinding binding;
    private PotriderApp mPotriderApp;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);

        mPotriderApp = (PotriderApp) getActivity().getApplication();




        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle("Profile", true, false, false,
                true, false, false, false);
        enableDisableFields(false);

        binding.topLL.setVisibility(View.GONE);

        binding.changePwdBTN.setOnClickListener(this);
        mainActivity.mEditImg.setOnClickListener(this);
        mainActivity.mSaveImg.setOnClickListener(this);
    }


    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mPotriderApp.isValidFName(binding.firstNameET)) {
            return false;
        }
        if (!mPotriderApp.isValidFName(binding.lastNameET)) {
            return false;
        }
        if (!mPotriderApp.isValidEmail(binding.emailET)) {
            return false;
        }
        if (!mPotriderApp.isValidPhone(binding.phoneNoET)) {
            return false;
        }
        if (!mPotriderApp.isValidMsg(binding.vModelET, getString(R.string.enter_vehicle_model))) {
            return  false;
        }
        if (!mPotriderApp.isValidMsg(binding.vNumberET, getString(R.string.enter_plate_number))) {
            return false;
        }
       /* if (mPotriderApp.isNetworkAvailable(getActivity())) {
            //doLoginService();
        } else {
            mPotriderApp.makeToast(getString(R.string.no_internet_connection), getActivity());
        }*/
        return true;
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.changePwdBTN:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new ChangePwdFragment(), true, false,
                        true, ChangePwdFragment.class.getSimpleName(), null, false);
                Bungee.slideRight(getActivity());
                break;

            case R.id.img_edit:
                enableDisableFields(true);
                binding.cameraIV.setVisibility(View.VISIBLE);
                setToolbarIconTitle("Profile", true, false, false,
                        false, false, true, false);
                break;

            case R.id.img_save:
                setToolbarIconTitle("Profile", true, false, false,
                        true, false, false, false);
                enableDisableFields(false);
                binding.cameraIV.setVisibility(View.GONE);
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        setanimationView(binding.topLL);
        mainActivity.hideKeyboard();
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    public void enableDisableFields(boolean is_enabled) {
        binding.firstNameET.setEnabled(is_enabled);
        binding.lastNameET.setEnabled(is_enabled);
        binding.emailET.setEnabled(is_enabled);
        binding.phoneNoET.setEnabled(is_enabled);
        binding.vModelET.setEnabled(is_enabled);
        binding.vNumberET.setEnabled(is_enabled);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle("Profile", true, false, false,
                    true, false, false, false);
        }
    }
}
