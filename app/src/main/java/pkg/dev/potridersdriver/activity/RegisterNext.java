package pkg.dev.potridersdriver.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.viewActivity.Home;
import pkg.dev.potridersdriver.app.PotriderApp;
import pkg.dev.potridersdriver.custom.CustomProgressDialog;
import pkg.dev.potridersdriver.databinding.ActivityRegisterNextBinding;
import spencerstudios.com.bungeelib.Bungee;

public class RegisterNext extends AppCompatActivity implements View.OnClickListener {


    ActivityRegisterNextBinding binding;
    private PotriderApp mPotriderApp;
    private CustomProgressDialog mProgressDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register_next);

        mPotriderApp = (PotriderApp) getApplication();
        mProgressDialog = new CustomProgressDialog().getInstance();




        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.mainLL.setVisibility(View.GONE);
        setAnimationView(binding.mainLL);

        binding.doneBTN.setOnClickListener(this);
        binding.backIV.setOnClickListener(this);
    }


    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mPotriderApp.isValidMsg(binding.modelET, getString(R.string.enter_vehicle_model))) {
            return false;
        }
        if (!mPotriderApp.isValidMsg(binding.plateNoET, getString(R.string.enter_plate_number))) {
            return false;
        }
        if (mPotriderApp.isNetworkAvailable(RegisterNext.this)) {
            //doLoginService();
        } else {
            mPotriderApp.makeToast(getString(R.string.no_internet_connection), RegisterNext.this);
        }

        return true;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backIV:
                mPotriderApp.hideKeyboard(RegisterNext.this);
                finish();
                Bungee.slideLeft(RegisterNext.this);
                break;

            case R.id.doneBTN:
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(binding.doneBTN);

                mPotriderApp.hideKeyboard(RegisterNext.this);
                if (checkValidation()){

                    mProgressDialog.showProgress(RegisterNext.this, "", true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(RegisterNext.this, Home.class));
                            finish();
                            Bungee.slideLeft(RegisterNext.this);
                        }
                    }, 3000);
                }
                mProgressDialog.dismiss();
                break;
        }
    }

    /**
     *  Method is used to initialized set animation....
     */
    public void setAnimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        setAnimationView(binding.mainLL);
    }
}
