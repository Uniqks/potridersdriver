package pkg.dev.potridersdriver.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.adapter.ViewPagerAdapter;
import pkg.dev.potridersdriver.databinding.FragmentMyOrdersBinding;


public class MyOrdersFragment extends BaseFragment {


    FragmentMyOrdersBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_orders, container, false);





        initControls();
        return binding.getRoot();
    }



    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle("My Orders", true, false, false,
                false, false, false, false);
        setupViewPager(binding.viewPager);
        binding.bubbleTab.setupWithViewPager(binding.viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new OngoingOrdersFragment(), "Ongoing Orders");
        adapter.addFrag(new PastOrdersFragment(), "Past Orders");
        viewPager.setAdapter(adapter);
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume() {
        super.onResume();
    }



    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden){
            setToolbarIconTitle("My Orders", true, false, false,
                    false, false, false, false);
        }
    }

}
