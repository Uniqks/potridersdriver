package pkg.dev.potridersdriver.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.databinding.FragmentTermsBinding;


public class TermsFragment extends BaseFragment {


    FragmentTermsBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms, container, false);






        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle(getString(R.string.terms), false, true, false,
                false, false, false, false);

        binding.topLL.setVisibility(View.GONE);
        setAnimationView(binding.topLL);
    }


    public void setAnimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_down);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onResume() {
        super.onResume();
        setAnimationView(binding.topLL);
    }
}
