package pkg.dev.potridersdriver.activity.viewActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import java.util.Arrays;
import java.util.HashMap;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.Login;
import pkg.dev.potridersdriver.activity.adapter.DrawerAdapter;
import pkg.dev.potridersdriver.activity.adapter.SimpleItem;
import pkg.dev.potridersdriver.activity.fragment.HelpFragment;
import pkg.dev.potridersdriver.activity.fragment.HomeFragment;
import pkg.dev.potridersdriver.activity.fragment.MyOrdersFragment;
import pkg.dev.potridersdriver.activity.fragment.ProfileFragment;
import pkg.dev.potridersdriver.activity.fragment.SettingsFragment;
import pkg.dev.potridersdriver.app.Constants;
import pkg.dev.potridersdriver.app.PotRiderSessionManager;
import pkg.dev.potridersdriver.model.DrawerItem;
import spencerstudios.com.bungeelib.Bungee;

public class Home extends BaseActivity implements View.OnClickListener, DrawerAdapter.OnItemSelectedListener {


    private static final int POS_HOME = 0;
    private static final int POS_ORDERS = 1;
    private static final int POS_SETTINGS = 2;
    private static final int POS_HELP = 3;
    private static final int POS_LOGOUT = 4;

    private String[] screenTitles;
    private Drawable[] screenIcons;

    public SlidingRootNav slidingRootNav;
    private RecyclerView mMenuList;

    boolean isEditClicked = false, isProfileClicked = false;
    boolean doubleBackToExitPressedOnce = false;


    public Toolbar mToolbar;
    public TextView mTitleTxt, mEmailTxt, mUnameTxt;
    public ImageView mProImg, mMenuImg, mFilterImg, mBackImg, mCartImg, mEditImg, mSaveImg, mLogoImg;


    CartEditProfileClick cartEditProfileClick;
    public interface CartEditProfileClick {
        void onEditIconClick(boolean isEdit);
    }

    public void setCartEditProfileClick(CartEditProfileClick cartEditProfileClick) {
        this.cartEditProfileClick = cartEditProfileClick;
    }

    private PotRiderSessionManager mAppManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mToolbar = findViewById(R.id.toolbar);


        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(mToolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(false)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();

        mAppManager = new PotRiderSessionManager(Home.this);





        initControls();
        setDrawerData();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mToolbar.setNavigationIcon(null);
        mTitleTxt = findViewById(R.id.txt_title);

        mBackImg = findViewById(R.id.imgBack);
        mBackImg.setOnClickListener(this);

        mLogoImg = findViewById(R.id.img_logo);

        mMenuImg = findViewById(R.id.img_menu);
        mMenuImg.setOnClickListener(this);

        mFilterImg = findViewById(R.id.img_filter);
        mFilterImg.setOnClickListener(this);

        mCartImg = findViewById(R.id.img_cart);
        mCartImg.setOnClickListener(this);

        mEditImg = findViewById(R.id.img_edit);
        mEditImg.setOnClickListener(this);

        mSaveImg = findViewById(R.id.img_save);
        mSaveImg.setOnClickListener(this);

        pushFragment(new HomeFragment(), false, true, false,
                HomeFragment.class.getSimpleName(), null, false);

        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();

        DrawerAdapter adapter = new DrawerAdapter(Arrays.asList(
                createItemFor(POS_HOME).setChecked(true),
                createItemFor(POS_ORDERS),
                createItemFor(POS_SETTINGS),
                createItemFor(POS_HELP),
                createItemFor(POS_LOGOUT)));
        adapter.setListener(this);

        mMenuList = findViewById(R.id.list_menu);
        mMenuList.setNestedScrollingEnabled(false);
        mMenuList.setLayoutManager(new LinearLayoutManager(this));
        mMenuList.setAdapter(adapter);


        findViewById(R.id.topHeaderLL).setOnClickListener(this);
        mUnameTxt = findViewById(R.id.txt_uname);
        mEmailTxt = findViewById(R.id.txt_email);
        mProImg = findViewById(R.id.img_pro);
    }

    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.colorPrimary))
                .withTextTint(color(R.color.colorWhite))
                .withSelectedIconTint(color(R.color.colorPrimary))
                .withSelectedTextTint(color(R.color.colorWhite));
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }

    private String[] loadScreenTitles() {
        if (!mAppManager.isUserLoggedIn()) {
            return getResources().getStringArray(R.array.ScreenLogout);
        } else {
            return getResources().getStringArray(R.array.ScreenLogin);
        }
    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta = getResources().obtainTypedArray(R.array.ld_activityScreenIcons);
        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(this, id);
            }
        }
        ta.recycle();
        return icons;
    }


    /**
     *  Method is used to initialized drawer data....
     */
    public void setDrawerData() {

        String name = "", propic = "", email = "";

        HashMap<String, String> user = mAppManager.getUserDetails();

        name = user.get(PotRiderSessionManager.FIRST_NAME + " " + PotRiderSessionManager.LAST_NAME);
        email = user.get(PotRiderSessionManager.EMAIL);
        propic = user.get(PotRiderSessionManager.PROPIC);

        if (!TextUtils.isEmpty(propic)) {
            Picasso.get().load(Constants.BASE_URL + propic).into(mProImg);
        }
        if (!TextUtils.isEmpty(name)) {
            mUnameTxt.setText("Sia Gracee");
        }
        if (!TextUtils.isEmpty(email)) {
            mEmailTxt.setText("sia@gmail.com");
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgBack:
                hideKeyboard();
                onBackPressed();
                slidingRootNav.setMenuLocked(false);
                break;

            case R.id.img_menu:
                hideKeyboard();
                slidingRootNav.openMenu();
                break;

            case R.id.topHeaderLL:
                slidingRootNav.closeMenu();
                isProfileClicked = true;

                performProfileClick();
                Bungee.slideRight(Home.this);
              /*  if (mAppManager.isUserLoggedIn()) {
                    performProfileClick();
                } else {
                    Intent intent = new Intent(Home.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    Bungee.slideRight(Home.this);
                }*/
                break;

            case R.id.img_cart:
                //slidingRootNav.setMenuLocked(true);
                //performCartClick();
                break;

            case R.id.img_filter:
                //startActivity(new Intent(this, AdvancedSearchActivity.class));
                break;
        }
    }

    @Override
    public void onItemSelected(int position) {

        if (position == 2) {

            slidingRootNav.closeMenu();
            isProfileClicked = false;
            getSupportFragmentManager().popBackStack();
            performSettingClick();
            Bungee.slideRight(Home.this);

        } else if (position == 0) {

            slidingRootNav.closeMenu();
            isProfileClicked = false;
            getSupportFragmentManager().popBackStack();
            performHomeClick();
            Bungee.slideRight(Home.this);

        } else if (position == 1) {

            slidingRootNav.closeMenu();
            isProfileClicked = false;
            getSupportFragmentManager().popBackStack();
            performMyOrderClick();
            Bungee.slideRight(Home.this);

        } else if(position == 3) {

            slidingRootNav.closeMenu();
            isProfileClicked = false;
            getSupportFragmentManager().popBackStack();
            performHelpClick();
            Bungee.slideRight(Home.this);

        } else if (position == 4) {

            slidingRootNav.closeMenu();
            final View dialogView = View.inflate(Home.this, R.layout.dialog_signout,null);

            final Dialog dialog = new Dialog(Home.this, R.style.MyAlertDialogStyle);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(dialogView);

            TextView mNoTxt, mYesTxt, mMessageTxt;

            mMessageTxt = dialog.findViewById(R.id.txt_msg);
            mMessageTxt.setText("Are you sure you want to Logout?");

            mNoTxt = dialog.findViewById(R.id.txt_no);
            mYesTxt = dialog.findViewById(R.id.txt_yes);

            mNoTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            mYesTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mAppManager.logoutUser();
                    Bungee.slideRight(Home.this);
                }
            });
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
            if (mAppManager.isUserLoggedIn()) {
            } else {
            }
        }
    }


    /**
     *  Method is used to initialized Order click event
     */
    private void performMyOrderClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(MyOrdersFragment.class.getSimpleName())) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        }
        pushFragment(new MyOrdersFragment(), true, false, true,
                MyOrdersFragment.class.getSimpleName(), null, false);
    }

    /**
     *  Method is used to initialized help click event.
     */
    private void performHelpClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(MyOrdersFragment.class.getSimpleName())) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        }
        pushFragment(new HelpFragment(), true, false, true,
                HelpFragment.class.getSimpleName(), null, false);
    }


    /**
     *  Method is used to initialized Home click event
     */
    private void performHomeClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(HomeFragment.class.getSimpleName())) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        }
        pushFragment(new HomeFragment(), false, true, false,
                HomeFragment.class.getSimpleName(), null, false);
    }

    /**
     *  Method is used to initialized Settings click event
     */
    private void performSettingClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(SettingsFragment.class.getSimpleName())) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        }
        pushFragment(new SettingsFragment(), true, false, true,
                SettingsFragment.class.getSimpleName(), null, false);
    }

    /**
     *  Method is used to initialized Profile click event
     */
    private void performProfileClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(ProfileFragment.class.getSimpleName())) {
                return;
            }
        }

        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        }

        pushFragment(new ProfileFragment(), true, false, true,
                ProfileFragment.class.getSimpleName(), null, false);
    }

    /**
     * Method is used to initialized Cart click event...
     */
    private void performCartClick() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(ProfileFragment.class.getSimpleName())) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
            getSupportFragmentManager().popBackStack();
        }
        /*pushFragment(new CartFragment(), true, false, true,
                CartFragment.class.getSimpleName(), null, false);*/
    }


    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {

            isEditClicked = false;
            slidingRootNav.closeMenu();
            getSupportFragmentManager().popBackStack();
            Bungee.slideLeft(Home.this);

            System.out.println("on  back --->" + getSupportFragmentManager().getBackStackEntryCount());

        } else {

            Bungee.slideLeft(Home.this);
            System.out.println("on  back1 --->" + getSupportFragmentManager().getBackStackEntryCount());
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;

            slidingRootNav.closeMenu();
            final View dialogView = View.inflate(Home.this, R.layout.dialog_signout,null);

            final Dialog dialog = new Dialog(Home.this, R.style.MyAlertDialogStyle);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(dialogView);

            TextView mNoTxt, mYesTxt, mMessageTxt;

            mMessageTxt = dialog.findViewById(R.id.txt_msg);
            mMessageTxt.setText("Are you sure you want to exit?");

            mNoTxt = dialog.findViewById(R.id.txt_no);
            mYesTxt = dialog.findViewById(R.id.txt_yes);

            mNoTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            mYesTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();


            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 3000);

        }
    }




    @Override
    protected void onResume() {
        super.onResume();
    }

}
