package pkg.dev.potridersdriver.activity.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.viewActivity.MapDirection;
import pkg.dev.potridersdriver.custom.HttpConnection;
import pkg.dev.potridersdriver.custom.PathJSONParser;
import pkg.dev.potridersdriver.databinding.FragmentHomeBinding;
import spencerstudios.com.bungeelib.Bungee;

import static android.content.Context.LOCATION_SERVICE;


public class HomeFragment extends BaseFragment implements OnMapReadyCallback,
        LocationListener,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {


    FragmentHomeBinding binding;

    private GoogleMap mMap;
    Location mLastLocation;
    Marker mCurrLocationMarker, mSelectedLocationMarker;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    protected LocationManager mLocationManager;
    boolean isGPSEnabled = false;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 100;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private String mGetDriverAddress = "";
    int height = 100;
    int width = 100;
    int dialogCount = 0;

    private Handler mHandler;
    public static boolean isReq = false;



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            setToolbarIconTitle("", true, false, false, false,
                    false, false, true);
        } else {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);





        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        setToolbarIconTitle("", true, false, false, false,
                false, false, true);

        if (!isReq) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    openRequestDialog();
                }
            }, 2000);
        }


        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mLocationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(isGPSEnabled) {
            GetLatLan();
        }else {
            showSettingsAlert_gps();
        }

        if (ActivityCompat.checkSelfPermission(mainActivity, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (mainActivity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,int[] grantResults)
            return;
        }

        if (ContextCompat.checkSelfPermission(mainActivity,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(mainActivity,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

            } else {

                ActivityCompat.requestPermissions(mainActivity,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
            }
        }
    }

    private final Runnable m_Runnable = new Runnable() {
        public void run() {
            dialogCount = dialogCount + 1;

            if (dialogCount < 3) {

                System.out.println("data counting ---->" + dialogCount);
                if (!isReq) {
                    openRequestDialog();
                }
                HomeFragment.this.mHandler.postDelayed(m_Runnable, 100000);
            } else {
            }
        }
    };

    private void openRequestDialog() {
        dialog = mainActivity.getDialog(true, mainActivity, R.layout.dialog_new_request);

        Button rejectBTN, acceptBTN;
        TextView detailsTV;

        rejectBTN = dialog.findViewById(R.id.rejectBTN);
        acceptBTN = dialog.findViewById(R.id.acceptBTN);
        detailsTV = dialog.findViewById(R.id.detailsTV);

        rejectBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.getWindow().getAttributes().windowAnimations = R.style.outDialogAnimation;
                dialog.dismiss();
            }
        });

        acceptBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.getWindow().getAttributes().windowAnimations = R.style.outDialogAnimation;
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new RequestDetailFragment(), true, false, true,
                        RequestDetailFragment.class.getSimpleName(), null, false);
                dialog.dismiss();

                mHandler = new Handler();
                mHandler.postDelayed(m_Runnable,80000);
            }
        });

        detailsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.getWindow().getAttributes().windowAnimations = R.style.outDialogAnimation;
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new RequestDetailFragment(), true, false, true,
                        RequestDetailFragment.class.getSimpleName(), null, false);
                dialog.dismiss();
                mHandler = new Handler();
                mHandler.postDelayed(m_Runnable,80000);
            }
        });
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        if (!hidden){
            setToolbarIconTitle("", true, false, false, false,
                    false, false, true);
        }
    }





    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mainActivity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mainActivity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(mainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(final Location location) {

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        BitmapDrawable bitmapdraw1 = (BitmapDrawable)getResources().getDrawable(R.drawable.markdriver);
        Bitmap b1 = bitmapdraw1.getBitmap();
        Bitmap curMarker = Bitmap.createScaledBitmap(b1, width, height, false);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
       // markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        //markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.trackmarker));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(curMarker));
        mCurrLocationMarker = mMap.addMarker(markerOptions);



        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            mGetDriverAddress = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

        } catch (IOException e) {
            e.printStackTrace();
        }


        BitmapDrawable bitmapdraw = (BitmapDrawable)getResources().getDrawable(R.drawable.markpotrider);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap resMarker = Bitmap.createScaledBitmap(b, width, height, false);
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(23.022505, 72.571365))
                .title("PotRider")
                .icon(BitmapDescriptorFactory.fromBitmap(resMarker))
        );

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(8));

        String url = getMapsApiDirectionsUrl();
        ReadTask downloadTask = new ReadTask();
        downloadTask.execute(url);

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void GetLatLan() {
        Log.e("GetLatLan","GetLatLan");
        checkPermission();
    }

    public boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_PHONE_STATE);
        int result1 = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int result2 = ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED ){

            LocationManager locationManager=(LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
            Criteria criteria=new Criteria();
            String  bestProvider=locationManager.getBestProvider(criteria, true);
            Location location=locationManager.getLastKnownLocation(bestProvider);
            if(location!=null) {
                onLocationChanged(location);
            } else {
            }

            locationManager.requestLocationUpdates(bestProvider, 10, 0, (android.location.LocationListener) this);
            return true;

        } else {
            requestPermission();
            return false;
        }
    }
    private void requestPermission(){
        ActivityCompat.requestPermissions(mainActivity, new String[]{android.Manifest.permission.READ_PHONE_STATE,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_REQUEST_CODE);
    }

    protected void showSettingsAlert_gps() {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(mainActivity);
        alertDialog.setTitle("GPS is settings");
        alertDialog.setMessage("GPS has not been enabled. To locate better friends for the setup menu and activate the GPS.  ");
        alertDialog.setPositiveButton("settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }


    private String getMapsApiDirectionsUrl() {
        String origin = "origin="+ mGetDriverAddress;
        String destination = "destination=" + "ahmedabad";
        String key = "key=" + getString(R.string.google_maps_key);

        String sensor = "sensor=false";
        String params = origin + "&" + destination + "&" + key;
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + params;


        //https://maps.googleapis.com/maps/api/directions/json?origin=sydney,au&destination=perth,au
        // &waypoints=via:-37.81223%2C144.96254%7Cvia:-34.92788%2C138.60008&key=YOUR_API_KEY


        System.out.println("get location url ---> " + url);
        return url;
    }


    private class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);

            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            new ParserTask().execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;


            if (routes != null && routes.size() != 0) {

                // traversing through routes
                for (int i = 0; i < routes.size(); i++) {
                    points = new ArrayList<LatLng>();
                    polyLineOptions = new PolylineOptions();
                    List<HashMap<String, String>> path = routes.get(i);

                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);

                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);

                        points.add(position);
                    }

                    polyLineOptions.addAll(points);
                    polyLineOptions.width(10);
                    polyLineOptions.geodesic(true);
                    polyLineOptions.color(getResources().getColor(R.color.colorMap));
                }

                mMap.addPolyline(polyLineOptions);
            } else {
                Toast.makeText(mainActivity, "no routes found", Toast.LENGTH_SHORT).show();
            }
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        //NewRequestDialog dialog = new NewRequestDialog();
        //dialog.show(getFragmentManager(), "new request");
    }
}
