package pkg.dev.potridersdriver.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.databinding.FragmentContactUsBinding;

public class ContactUsFragment extends BaseFragment implements View.OnClickListener {


    FragmentContactUsBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false);




        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls
     */
    private void initControls() {

        setToolbarIconTitle(getString(R.string.contact_us), false, true, false,
                false, false, false, false);

        binding.topLL.setVisibility(View.GONE);
        setanimationView(binding.topLL);
        binding.sendBT.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.sendBT:
                mainActivity.hideKeyboard();
                Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity, R.anim.slide_down);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

}
