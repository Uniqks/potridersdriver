package pkg.dev.potridersdriver.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.viewActivity.MapDirection;
import pkg.dev.potridersdriver.app.PotRiderSessionManager;
import pkg.dev.potridersdriver.databinding.ActivitySplaceAlertBinding;
import spencerstudios.com.bungeelib.Bungee;

public class SplaceAlert extends AppCompatActivity implements View.OnClickListener {


    ActivitySplaceAlertBinding binding;
    PotRiderSessionManager mAppManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splace_alert);

        mAppManager = new PotRiderSessionManager(SplaceAlert.this);





        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.noBTN.setOnClickListener(this);
        binding.yesBTN.setOnClickListener(this);

        setanimationView(binding.logoIV);
        setanimationView(binding.alertLL);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.noBTN:
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(binding.noBTN);

                /*startActivity(new Intent(SplaceAlert.this, Login.class));
                finish();*/
                startActivity(new Intent(SplaceAlert.this, MapDirection.class));
                finish();
                Bungee.slideLeft(SplaceAlert.this);
                break;

            case R.id.yesBTN:
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(binding.yesBTN);

                startActivity(new Intent(SplaceAlert.this, Login.class));
                finish();
                Bungee.slideLeft(SplaceAlert.this);
                break;
        }
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(SplaceAlert.this, R.anim.slide_down);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }
}
