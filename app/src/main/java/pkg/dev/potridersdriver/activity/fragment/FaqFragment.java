package pkg.dev.potridersdriver.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.databinding.FragmentFaqBinding;
import pkg.dev.potridersdriver.model.FaqDetails;


public class FaqFragment extends Fragment {


    FragmentFaqBinding binding;
    private List<FaqDetails> mFaqDetails = new ArrayList<>();
    private FaqListAdapter mFaqListAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_faq, container, false);






        initControls();
        setFaqListAdapter();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.faqsRV.setHasFixedSize(true);
        binding.faqsRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        mFaqDetails.add(new FaqDetails("1", "Getting Started", getString(R.string.str_dummy)));
        mFaqDetails.add(new FaqDetails("2", "Shopping Questions", getString(R.string.str_dummy)));
        mFaqDetails.add(new FaqDetails("3", "Payment Questions", getString(R.string.str_dummy)));
    }

    /**
     *  Method is used to set faq list adapter...
     */
    private void setFaqListAdapter() {
        mFaqListAdapter = new FaqListAdapter(mFaqDetails, FaqFragment.this);
        binding.faqsRV.setAdapter(mFaqListAdapter);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setanimationView(binding.faqsRV);
            }
        }, 500);

    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    /**
     *  Faq list adapter...
     */
    public class FaqListAdapter extends RecyclerView.Adapter<FaqListAdapter.ViewHolder> {

        private List<FaqDetails> mData;
        private Fragment fragment;

        public FaqListAdapter(List<FaqDetails> mData, Fragment fragment) {
            this.mData = mData;
            this.fragment = fragment;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_faq_list, viewGroup, false);



            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            if (mData.get(position).getTitle() != null && !mData.get(position).getTitle().equals("")) {
                holder.mTitleTV.setText(mData.get(position).getTitle());
            } else {
            }

            if (mData.get(position).getDesc() != null && !mData.get(position).getDesc().equals("")) {
                 holder.mDescTV.setText(mData.get(position).getDesc());
            } else {
            }
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView mTitleTV, mDescTV;

            public ViewHolder(View view) {
                super(view);

                mTitleTV = view.findViewById(R.id.titleTV);
                mDescTV = view.findViewById(R.id.descTV);
            }
        }
    }
}
