package pkg.dev.potridersdriver.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.viewActivity.Home;
import pkg.dev.potridersdriver.app.PotriderApp;
import pkg.dev.potridersdriver.custom.CustomProgressDialog;
import pkg.dev.potridersdriver.databinding.ActivityRegisterBinding;
import spencerstudios.com.bungeelib.Bungee;

public class Register extends AppCompatActivity implements View.OnClickListener{


    ActivityRegisterBinding binding;
    private PotriderApp mPotriderApp;
    private CustomProgressDialog mProgressDialog = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);

        mPotriderApp = (PotriderApp) getApplication();
        mProgressDialog = new CustomProgressDialog().getInstance();




        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        mProgressDialog.dismiss();
        binding.mainLL.setVisibility(View.GONE);
        setAnimationView(binding.mainLL);

        binding.nextBTN.setOnClickListener(this);
        binding.loginLL.setOnClickListener(this);
        binding.backIV.setOnClickListener(this);


        String strTerms = "By clicking this box, I have read the " + "<b>" + "Terms & Conditions " + "</b>" + "of this app.";
        binding.termsTV.setText(Html.fromHtml(strTerms));
        binding.termsTV.setOnClickListener(this);
    }



    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mPotriderApp.isValidFName(binding.fnameET)) {
            return false;
        }
        if (!mPotriderApp.isValidLName(binding.lnameET)) {
            return false;
        }
        if (!mPotriderApp.isValidEmail(binding.emailET)) {
            return false;
        }
        if (!mPotriderApp.isValidPhone(binding.phoneET)) {
            return false;
        }
        if (!mPotriderApp.isValidatePassword(binding.pwdET)) {
            return false;
        }
        if (!binding.termsCheck.isChecked()) {
            Toast.makeText(Register.this, getString(R.string.err_accept_terms_conditions), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (mPotriderApp.isNetworkAvailable(Register.this)) {
            //doLoginService();
        } else {
            mPotriderApp.makeToast(getString(R.string.no_internet_connection), Register.this);
        }

        return true;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backIV:
                mPotriderApp.hideKeyboard(Register.this);
                Intent intent = new Intent(Register.this, Login.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                Bungee.slideLeft(Register.this);
                break;

            case R.id.termsTV:
                mPotriderApp.hideKeyboard(Register.this);
                startActivity(new Intent(Register.this, TermsConditions.class));
                Bungee.slideLeft(Register.this);
                break;

            case R.id.loginLL:
                mPotriderApp.hideKeyboard(Register.this);
                Intent intent1 = new Intent(Register.this, Login.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                finish();
                Bungee.slideLeft(Register.this);
                break;

            case R.id.nextBTN:
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .repeat(1)
                        .playOn(binding.nextBTN);

                mPotriderApp.hideKeyboard(Register.this);
                if (checkValidation()){

                    //mProgressDialog.showProgress(Register.this, "", true);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(Register.this, RegisterNext.class));
                            Bungee.slideLeft(Register.this);
                            mProgressDialog.dismiss();
                        }
                    }, 3000);
                }
                break;
        }
    }


    /**
     *  Method is used to initialized set animation....
     */
    public void setAnimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        mProgressDialog.dismiss();
        setAnimationView(binding.mainLL);
    }

}
