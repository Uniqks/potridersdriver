package pkg.dev.potridersdriver.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.activity.Login;
import pkg.dev.potridersdriver.app.PotriderApp;
import pkg.dev.potridersdriver.databinding.FragmentChangePwdBinding;


public class ChangePwdFragment extends BaseFragment implements View.OnClickListener {


    FragmentChangePwdBinding binding;
    private PotriderApp mPotriderApp;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_pwd, container, false);

        mPotriderApp = (PotriderApp) getActivity().getApplication();




        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls..
     */
    private void initControls() {

        setToolbarIconTitle("Change Password", false, true, false,
                false, false, false, false);

        binding.topLL.setVisibility(View.GONE);
        setanimationView(binding.topLL);
        binding.doneBTN.setOnClickListener(this);
    }


    /**
     *  Method is used to initialized check validation...
     */
    private boolean checkValidation() {
        if (!mPotriderApp.isValidateCurrentPassword(binding.currentPwdET)) {
            return false;
        }
        if (!mPotriderApp.isValidNewPassword(binding.newPwdET)) {
            return false;
        }
        if (!mPotriderApp.isValidConfirmPassword(binding.newPwdET, binding.confirmPwdET)) {
            return false;
        }
        if (mPotriderApp.isNetworkAvailable(getActivity())) {
            //doLoginService();
        } else {
            mPotriderApp.makeToast(getString(R.string.no_internet_connection), getActivity());
        }

        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.doneBTN:
                mainActivity.hideKeyboard();
                checkValidation();
                /*binding.currentPwdET.setText("");
                binding.newPwdET.setText("");
                binding.confirmPwdET.setText("");*/
                break;
        }
    }

    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(mainActivity, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.hideKeyboard();
    }

}
