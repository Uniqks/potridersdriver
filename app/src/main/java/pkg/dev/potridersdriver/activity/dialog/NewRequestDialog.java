package pkg.dev.potridersdriver.activity.dialog;


import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.databinding.DialogNewRequestBinding;
import spencerstudios.com.bungeelib.Bungee;


public class NewRequestDialog extends DialogFragment implements View.OnClickListener {


    DialogNewRequestBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_new_request, container, false);
        getDialog().getWindow().requestFeature(1);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.enterDialogAnimation;




        initControls();
        return binding.getRoot();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        //binding.topRl.setVisibility(View.GONE);
        //setAnimationDownView(binding.topRl);

        binding.rejectBTN.setOnClickListener(this);
        binding.acceptBTN.setOnClickListener(this);
        binding.detailsTV.setOnClickListener(this);
    }

    /**
     *  Method is used to initialized animation...
     */
    public void setAnimationDownView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.zoom_enter);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.rejectBTN:
                getDialog().getWindow().getAttributes().windowAnimations = R.style.outDialogAnimation;
                getDialog().dismiss();
                break;

            case R.id.acceptBTN:
                getDialog().getWindow().getAttributes().windowAnimations = R.style.outDialogAnimation;
                getDialog().dismiss();
               /* YoYo.with(Techniques.ZoomOut)
                        .duration(700)
                        .repeat(0)
                        .playOn(binding.topRl);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getDialog().dismiss();
                    }
                }, 700);*/
                break;

            case R.id.detailsTV:
                Toast.makeText(getActivity(), "Work in progress...", Toast.LENGTH_SHORT).show();
                getDialog().getWindow().getAttributes().windowAnimations = R.style.outDialogAnimation;
                getDialog().dismiss();
                break;
        }
    }
}
