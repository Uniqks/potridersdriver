package pkg.dev.potridersdriver.activity.viewActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import pkg.dev.potridersdriver.R;

/**
 * Created by Seema on 27/11/18.
 */

public class BaseActivity extends AppCompatActivity {

    public Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initProgressDialog();
    }

    public void pushFragment(Fragment fragment, boolean isAdd, boolean isReplace, boolean isAddToBackstack, String tag, Bundle bundle, boolean isRefresh) {

        if (bundle != null) {
            fragment.setArguments(bundle);
        }


        FragmentManager fragmentManager = getSupportFragmentManager();
        // Or: FragmentManager fragmentManager = getSupportFragmentManager() fro below 4.0 support
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


        //if (!isRefresh) {
        if (getCurrentFragment() != null) {
            if (getCurrentFragment().getTag().equalsIgnoreCase(tag)) {
                return;
            }
        }
        //}

        //determine animation
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (isAdd) {
            //add fragments
            fragmentTransaction.add(R.id.container, fragment, tag);
        } else if (isReplace) {
            //replace fragments
            fragmentTransaction.replace(R.id.container, fragment, tag);
        } else {
        }

        //determine backstack
        if (isAddToBackstack) {
            fragmentTransaction.addToBackStack(tag);
        } else {
        }


        //hide keyboard
        hideKeyboard();

        if (getCurrentFragment() != null) {
            fragmentTransaction.hide(getCurrentFragment());
        }

        if (!isFinishing()) {
            fragmentTransaction.commitAllowingStateLoss();
        }
    }

    public Fragment getCurrentFragment() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm != null) {
            return fm.findFragmentById(R.id.container);
        } else {
            return null;
        }
    }


    //show keyboard
    public void showKeyboard(View view) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
        }
    }

    //hide keyboard
    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            view.clearFocus();
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void initProgressDialog() {
        dialog = new Dialog(BaseActivity.this);
       // View view = LayoutInflater.from(this).inflate(R.layout.layout_progress_dialog, null);
        //dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //dialog.setMessage(mContext.getString(R.string.api_loader_msg));
        dialog.setCancelable(false);
    }

    public void showProgressDialog() {
        if (dialog != null && !dialog.isShowing())
            dialog.show();
    }

    public void hideProgressDialog() {
        if (dialog != null && dialog.isShowing()) dialog.dismiss();
    }


    public Dialog getDialog(boolean isShow, Activity sActivity, int layout) {

        Dialog dialog = new Dialog(sActivity);
        if (isShow) {
            dialog.setContentView(layout);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            dialog.getWindow().setLayout(-1, -2);
            dialog.getWindow().getAttributes().windowAnimations = R.style.enterDialogAnimation;
            dialog.getWindow().setGravity(17);
            dialog.show();

        } else {
            dialog.dismiss();
        }
        return dialog;
    }

}

