package pkg.dev.potridersdriver.activity;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.databinding.ActivityTermsConditionsBinding;

public class TermsConditions extends AppCompatActivity implements View.OnClickListener {


    ActivityTermsConditionsBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_terms_conditions);





        initControls();
    }


    /**
     *  Method is used to initialized all the controls...
     */
    private void initControls() {

        binding.topLL.setVisibility(View.GONE);
        setanimationView(binding.topLL);

        binding.backIV.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.backIV:
                onBackPressed();
                break;
        }
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(TermsConditions.this, R.anim.slide_left_right);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }
}
