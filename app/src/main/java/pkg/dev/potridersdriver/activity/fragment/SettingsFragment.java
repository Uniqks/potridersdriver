package pkg.dev.potridersdriver.activity.fragment;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import pkg.dev.potridersdriver.R;
import pkg.dev.potridersdriver.databinding.FragmentSettingsBinding;
import spencerstudios.com.bungeelib.Bungee;


public class SettingsFragment extends BaseFragment implements View.OnClickListener {


    FragmentSettingsBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);








        initControls();
        return binding.getRoot();
    }


    /**
     *   Method is used to initialized all the controls..
     */
    private void initControls() {

        setToolbarIconTitle("Settings", true, false, false, false,
                false, false, false);

        binding.topLL.setVisibility(View.GONE);
        setanimationView(binding.topLL);

        binding.privacyPolicyRL.setOnClickListener(this);
        binding.aboutUsRL.setOnClickListener(this);
        binding.contactUsRL.setOnClickListener(this);
        binding.termsRL.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.privacyPolicyRL:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new PrivacyFragment(), true, false, true,
                        PrivacyFragment.class.getSimpleName(), null, false);
                Bungee.slideLeft(getActivity());
                break;

            case R.id.aboutUsRL:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new AboutUsFragment(), true, false, true,
                        AboutUsFragment.class.getSimpleName(), null, false);
                break;

            case R.id.contactUsRL:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new ContactUsFragment(), true, false, true,
                        ContactUsFragment.class.getSimpleName(), null, false);
                break;

            case R.id.termsRL:
                mainActivity.slidingRootNav.setMenuLocked(true);
                mainActivity.pushFragment(new TermsFragment(), true, false, true,
                        TermsFragment.class.getSimpleName(), null, false);
                break;
        }
    }


    public void setanimationView(View topLL) {
        Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_up);
        topLL.startAnimation(bottomUp);
        topLL.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setToolbarIconTitle("Settings", true, false, false, false,
                    false, false, false);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        setToolbarIconTitle("Settings", true, false, false, false,
                false, false, false);
    }
}
