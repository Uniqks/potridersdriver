package pkg.dev.potridersdriver.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;

import java.util.HashMap;

import pkg.dev.potridersdriver.activity.Login;

/**
 * Created by Seema on 26/11/18.
 */

public class PotRiderSessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    Fragment fragment;

    int PRIVATE_MODE = 0;


    private static final String PREFER_NAME = "Potriders Driver";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";
    public static final String IS_STARTED = "IS_STARTED";
    public static final String IS_STAY_SIGNIN = "isstaysignedin";
    public static final String IS_FROM_MAIN_ACTIVITY = "IS_FROM_MAIN_ACTIVITY";
    public static final String USER_ID = "id";
    public static final String USER_NAME = "username";
    public static final String FIRST_NAME = "fname";
    public static final String LAST_NAME = "lname";
    public static final String EMAIL = "email";
    public static final String PHONE = "phone";
    public static final String PROPIC = "image";
    public static final String PWD = "pwd";
    public static final String TOKEN = "token";

    public PotRiderSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public PotRiderSessionManager(Fragment fragment) {
        this.fragment = fragment;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createUserLoginSession(String id, String fname, String lname, String email, String phone, String img, String token) {
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putString(USER_ID, id);
        editor.putString(FIRST_NAME, fname);
        editor.putString(LAST_NAME, lname);
        editor.putString(EMAIL, email);
        editor.putString(PHONE, phone);
        editor.putString(PROPIC, img);
        editor.putString(TOKEN, token);
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        HashMap<String, Boolean> isSignIn = new HashMap<String, Boolean>();
        user.put(USER_ID, pref.getString(USER_ID, null));
        user.put(FIRST_NAME, pref.getString(FIRST_NAME, null));
        user.put(LAST_NAME, pref.getString(LAST_NAME, null));
        user.put(EMAIL, pref.getString(EMAIL, null));
        user.put(PHONE, pref.getString(PHONE, null));
        user.put(PROPIC, pref.getString(PROPIC, null));
        user.put(TOKEN, pref.getString(TOKEN, null));
        return user;
    }

    public HashMap<String, Boolean> getSignInDetails() {
        HashMap<String, Boolean> isSignIn = new HashMap<String, Boolean>();
        isSignIn.put(IS_STAY_SIGNIN, pref.getBoolean(IS_STAY_SIGNIN, false));
        return isSignIn;
    }

    public void logoutUser() {
        editor.clear();
        editor.commit();

        Intent i = new Intent(_context, Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }


    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGIN, false);
    }
}
