package pkg.dev.potridersdriver.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import pkg.dev.potridersdriver.R;


/**
 * Created by Seema on 26/11/18.
 */

public class PotriderApp extends Application {

    private ArrayList<WeakReference<OnLowMemoryListener>> mLowMemoryListeners;
    private ExecutorService mExecutorService;
    private static PotriderApp mInstance = null;
    private static final int CORE_POOL_SIZE = 5;
    private static Context context;

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    private static String[] MULTIMEDIA_PERMISSIONS = new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.CAMERA"};

    private static String[] LOCATION_PERMISSIONS = new String[] {"android.permission.READ_PHONE_STATE", "android.permission.ACCESS_COARSE_LOCATION", "android.permission.ACCESS_FINE_LOCATION"};


    @Override
    public void onCreate() {

        super.onCreate();

        context = null;
        context = getApplicationContext();


        mLowMemoryListeners = new ArrayList<WeakReference<OnLowMemoryListener>>();
        mInstance = this;

    }

    public static Context getGlobalContext() {
        return context;
    }

    public static Resources getAppResources() {
        return context.getResources();
    }

    public static String getAppString(int resourceId, Object... formatArgs) {
        return getAppResources().getString(resourceId, formatArgs);
    }

    public static String getAppString(int resourceId) {
        return getAppResources().getString(resourceId);
    }

    @Override
    public void onTerminate() {


        super.onTerminate();
    }

    public static interface OnLowMemoryListener {

        /**
         * Callback to be invoked when the system needs memory.
         */
        public void onLowMemoryReceived();
    }

    public static boolean checkMultimediaPermission(Activity activity) {
        int readPermission = ContextCompat.checkSelfPermission(activity, "android.permission.READ_EXTERNAL_STORAGE");
        int writePermission = ContextCompat.checkSelfPermission(activity, "android.permission.WRITE_EXTERNAL_STORAGE");
        int camera = ContextCompat.checkSelfPermission(activity, "android.permission.CAMERA");
        if (readPermission == 0 && writePermission == 0 && camera == 0) {
            return true;
        }
        ActivityCompat.requestPermissions(activity, MULTIMEDIA_PERMISSIONS, 1);
        return false;
    }

    public static boolean checkLocationPermission(Activity activity) {
        int readPhonePermission = ContextCompat.checkSelfPermission(activity, "android.permission.READ_PHONE_STATE");
        int accessCoarsePermission = ContextCompat.checkSelfPermission(activity, "android.permission.ACCESS_COARSE_LOCATION");
        int accessfinePermission = ContextCompat.checkSelfPermission(activity, "android.permission.ACCESS_FINE_LOCATION");

        if (readPhonePermission == 0 && accessCoarsePermission == 0 && accessfinePermission == 0) {
            return true;
        }
        ActivityCompat.requestPermissions(activity, LOCATION_PERMISSIONS, 1);
        return false;
    }

    public void registerOnLowMemoryListener(OnLowMemoryListener listener) {
        if (listener != null) {
            mLowMemoryListeners.add(new WeakReference<OnLowMemoryListener>(
                    listener));
        }
    }

    public void unregisterOnLowMemoryListener(OnLowMemoryListener listener) {
        if (listener != null) {
            int i = 0;
            while (i < mLowMemoryListeners.size()) {
                final OnLowMemoryListener l = mLowMemoryListeners.get(i).get();
                if (l == null || l == listener) {
                    mLowMemoryListeners.remove(i);
                } else {
                    i++;
                }
            }
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        int i = 0;
        while (i < mLowMemoryListeners.size()) {
            final OnLowMemoryListener listener = mLowMemoryListeners.get(i)
                    .get();
            if (listener == null) {
                mLowMemoryListeners.remove(i);
            } else {
                listener.onLowMemoryReceived();
                i++;
            }
        }
    }

    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
        private final AtomicInteger mCount = new AtomicInteger(1);

        public Thread newThread(Runnable r) {
            return new Thread(r, "GreenDroid thread #"
                    + mCount.getAndIncrement());
        }
    };

    public ExecutorService getExecutor() {
        if (mExecutorService == null) {
            mExecutorService = Executors.newFixedThreadPool(CORE_POOL_SIZE,
                    sThreadFactory);
        }
        return mExecutorService;
    }

    public static PotriderApp getInstance() {
        return mInstance;
    }




    public void hideSoftKeyBoard(View view){
        try {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }




    public boolean isValidEmail(EditText editText) {
        if (!Validation.hasText(editText, getString(R.string.err_empty_email))) {
            return false;
        }
        return Validation.isEmailAddress(editText, true, getString(R.string.error_email_invalid));
    }

    public boolean isValidatePassword(EditText editText) {
        if (!Validation.hasText(editText, getString(R.string.err_empty_password))) {
            return false;
        }
        return Validation.isValidPassword(editText, getString(R.string.error_pass_invalid));
    }

    public boolean isValidConfirmPassword(EditText nPwdEdt, EditText cPwdEdt) {
        if (!Validation.hasText(cPwdEdt, getString(R.string.err_confirm_pwd))) {
            return false;
        }
        if (!Validation.isValidPassword(cPwdEdt, getString(R.string.error_pass_invalid))) {
            return false;
        }
        return Validation.isConfirmPassword(nPwdEdt, cPwdEdt, getString(R.string.password_not_match));
    }

    public boolean isValidateCurrentPassword(EditText editText) {
        if (!Validation.hasText(editText, getString(R.string.enter_current_password))) {
            return false;
        }
        return Validation.isValidPassword(editText, getString(R.string.error_pass_invalid));
    }

    public boolean isValidNewPassword(EditText editText) {
        if (!Validation.hasText(editText, getString(R.string.enter_new_password))) {
            return false;
        }
        return Validation.isValidPassword(editText, getString(R.string.error_pass_invalid));
    }


    public boolean isValidPhone(EditText editText) {
        return Validation.hasText(editText, getString(R.string.err_empty_phone_number));
    }

    public boolean isValidLName(EditText editText) {
        return Validation.hasText(editText, getString(R.string.err_empty_lastname));
    }

    public boolean isValidFName(EditText editText) {
        return Validation.hasText(editText, getString(R.string.err_empty_firstname));
    }

    public boolean isValidMsg(EditText editText, String message) {
        return Validation.hasText(editText, message);
    }




    public static boolean isNetworkAvailable(Context context) {

        int conn = getConnectivityStatus(context);

        if (conn == TYPE_WIFI) {
            //status = "Wifi enabled";
            //status="Internet connection available";
            return true;
        } else if (conn == TYPE_MOBILE) {
            //status = "Mobile data enabled";
            //status="Internet connection available";
            return true;
        } else if (conn == TYPE_NOT_CONNECTED) {
            //status = "Not connected to Internet";
            return false;
        }
        return false;
    }

    public static int getConnectivityStatus(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork) {

            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }


    public static String getFormattedDateOfStay(String oldDateFormat, String dates) {

        String convertedDates = "";
        Date dateNew;
        SimpleDateFormat df = new SimpleDateFormat(oldDateFormat);
        try {
            dateNew = df.parse(dates);

            Calendar cal = Calendar.getInstance();
            cal.setTime(dateNew);
            //2nd of march 2015
            int day = cal.get(Calendar.DATE);

            switch (day) {
                case 1:
                case 21:
                case 31:
                    return new SimpleDateFormat("MMMM d'st', yyyy").format(dateNew);
                case 2:
                case 22:
                    return new SimpleDateFormat("MMMM d'nd', yyyy").format(dateNew);
                case 3:
                case 23:
                    return new SimpleDateFormat("MMMM d'rd', yyyy").format(dateNew);
                default:
                    return new SimpleDateFormat("MMMM d'th', yyyy").format(dateNew);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return convertedDates;
    }

    public static void makeToast(String message, Context mContext) {
        try {
            Toast toast = Toast.makeText(mContext, "" + message, Toast.LENGTH_SHORT);
            View view = toast.getView();
            //view.setBackgroundResource(R.drawable.toast_bg);
            TextView text = (TextView) view.findViewById(android.R.id.message);
            //text.setTextColor(getColor(mContext, R.color.white));
            toast.setGravity(Gravity.BOTTOM, 0, 190);
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

