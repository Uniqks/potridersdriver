package pkg.dev.potridersdriver.custom;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import pkg.dev.potridersdriver.R;

/**
 * Created by Seema on 26/11/18.
 */

public class CustomProgressDialog {

    private Context context;
    private ProgressDialog dialog;

    public static CustomProgressDialog customProgress = null;
    private Dialog mDialog;
    private ProgressBar mProgressBar;
    private TextView mProgressTxt;


    /*public CustomProgressDialog(Context context){
        try {

            dialog = new ProgressDialog(context);
            dialog.setCancelable(false);
            dialog.setMessage("Loading...");


            // dialog = new ProgressDialog(context,R.style.CustomDialog);
            // dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public static CustomProgressDialog getInstance() {
        if (customProgress == null) {
            customProgress = new CustomProgressDialog();
        }
        return customProgress;
    }


    public void show(){
        try {
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showProgress(Context context, String message, boolean cancelable) {
        mDialog = new Dialog(context);
        // no tile for the dialog
        mDialog.getWindow().requestFeature(1);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mDialog.setContentView(R.layout.progress_spin_dialog);
        mProgressBar = mDialog.findViewById(R.id.progressBar);

        //  mProgressBar.getIndeterminateDrawable().setColorFilter(context.getResources()
        // .getColor(R.color.material_blue_gray_500), PorterDuff.Mode.SRC_IN);
       /* mProgressTxt = (TextView) mDialog.findViewById(R.id.progress_text);
        mProgressTxt.setText("" + message);
        mProgressTxt.setVisibility(View.VISIBLE);*/
        mProgressBar.setVisibility(View.VISIBLE);
        // you can change or add this line according to your need
        mProgressBar.setIndeterminate(true);
        //mDialog.setCancelable(cancelable);
        //mDialog.setCanceledOnTouchOutside(cancelable);
        mDialog.show();
    }

    public void dismiss(){
        try {
            if (mDialog != null && mDialog.isShowing()) {
                mDialog.dismiss();
                mDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
